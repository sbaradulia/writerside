# Plain MD

## Ampersand

Not escaped &amp; should be visible as &
{id="not-escaped-ampersand"}

HTML entity `&amp;` should be visible as code of &amp;
{id="html-entity-escaped-ampersand"}

HTML entity `&amp;amp;` (code &amp;amp;) should be visible as code
{id="html-entity-escaped-double-ampersand"}

HTML code `&#38;` should be visible as code of &#38;
{id="html-code-escaped-ampersand"}

UNICODE `U+00026` should be visible as code U+00026
{id="unicode-escaped-ampersand"}

HEX code `&#x26;` should be visible as code of &#x26;
{id="hex-escaped-ampersand"}

CSS code `\0026` should be visible as code \0026
{id="css-escaped-ampersand"}

## Letters and symbols

Simple code `<aaa, bbb>` (<aaa, bbb>) should be visible as code
{id="simple-code-escaped"}

Code `<aaa>text</aaa>` and text <aaa>text</aaa> should be visible as code only
{id="simple-tag-text-code-escaped"}

Not escaped &#124; should be visible as |
{id="not-escaped-vertical-bar"}

HTML code of `&#124;` (vertical bar &#124;) should be visible as code
{id="html-code-escaped-vertical-bar"}

HTML code `&#65;` (letter &#65;) should be visible as code
{id="html-code-escaped-a"}

HTML code `&#22291;` (hieroglyph &#22291;) should be visible as code
{id="html-code-escaped-hieroglyph"}

HTML code `&#8594;` (arrow &#8594;) should be visible as code
{id="html-code-escaped-right-arrow"}

HTML code `&#190;` (num &#190;) should be visible as code
{id="html-code-escaped-34num"}

HTML code `&#35;` (symbol &#35;) should be visible as code
{id="html-code-escaped-sharp"}

HTML code `&#64;` (symbol &#64;) should be visible as code
{id="html-code-escaped-at"}

HTML code `&#174;` (symbol &#174;) should be visible as code
{id="html-code-escaped-trade-mark"}

HTML code `&#9757;` (symbol &#9757;) should be visible as code
{id="html-code-escaped-finger"}

HTML code `&#128514;` (smile &#128514;) should be visible as code
{id="html-code-escaped-smile"}

HTML entity `&Ntilde;` (letter &Ntilde;) should be visible as code
{id="html-entity-escaped-n"}

HTML entity `&pound;` (symbol &pound;) should be visible as code
{id="html-entity-escaped-pound"}

HTML entity `&pi;` (letter &pi;) should be visible as code
{id="html-entity-escaped-pi"}

HTML entity `&lt;` (arrow &lt;) should be visible as code
{id="html-entity-escaped-left-arrow"}

HTML entity `&percnt;` (symbol &percnt;) should be visible as code
{id="html-entity-escaped-percent"}

HTML entity `&copy;` (symbol &copy;) should be visible as code
{id="html-entity-escaped-copyright"}

HTML entity `&phone;` (symbol &phone;) should be visible as code
{id="html-entity-escaped-phone"}

HEX code `&#x2152;` (num &#x2152;) should be visible as code
{id="hex-escaped-10num"}

HEX code `&#x570E;` (hieroglyph &#x570E;) should be visible as code
{id="hex-escaped-hieroglyph"}
