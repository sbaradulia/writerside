[//]: # (title: Image attributes MD)

## Image without attributes

![No attributes image](editing_layer.png)

## Border-effect

![Rounded image](book.jpg){border-effect="rounded"}

## Width

Image 100px width:

![Image width 100](book.jpg){width="100"}

Image 0px width:

![Image width 0](book.jpg){width="0"}


## Height

Image 200px height:

![Image height 200](book.jpg){height="200"}

## Style

Inline: ![Inline image](book.jpg){style="inline" width="100"}

Block:![Block image](book.jpg){style="block" height="22"}

## Thumbnail

True:

![Thumbnail true image](book.jpg){thumbnail="true"}

False:

![Thumbnail false image](book.jpg){thumbnail="false"}

## Preview-src

![Another picture preview](book.jpg){preview-src="shiprock.jpeg"}
