[//]: # (title: Definition list attributes MD)

## Style compact

Unit Testing
: Definition for "Unit Testing". This initial stage in testing normally carried out by the developer who wrote the code and sometimes by a peer using the white box testing technique.
{style="compact"}

##  Style narrow

Integration Testing
: Definition for "Integration Testing". This stage is carried out in two modes, as a complete package or as an increment to the earlier package. Most of the time black box testing technique is used. However, sometimes a combination of Black and White box testing is also used in this stage. {style="narrow"}

## Style medium

System Testing
: Definition for "System Testing". In this stage the software is tested from all possible dimensions for all intended purposes and platforms. In this stage Black box testing technique is normally used.
: Another definition for "System Testing". System testing, also referred to as system-level tests or system-integration testing, is the process in which a quality assurance (QA) team evaluates how the various components of an application interact together in the full, integrated system or application.
{style="medium"}

## Style wide

User Acceptance Testing
: Definition for "User Acceptance Testing". This testing stage carried out in order to get customer sign-off of finished product. A 'pass' in this stage also ensures that the customer has accepted the software and is ready for their use.
{style="wide"}

