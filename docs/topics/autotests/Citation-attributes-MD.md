[//]: # (title: Citation attributes MD)

##  Style tip

> First line
> Second line
>
{style="tip" id="data-type-tip"}

## Style note

> First line
> Second line
>
{style="note" id="data-type-note"}

## Style warning

> First line
> Second line
>
{style="warning" id="data-type-warning"}
