# Video MD

## Youtube full link without title

<video src="https://youtube.com/watch?v=zDP9uUMYrvs" id="youtube-full-without-title"/>

## Youtube full link with title

<video src="https://youtube.com/watch?v=zDP9uUMYrvs" title="How We Built Comma" id="youtube-full-with-title"/>

## Youtube full link with title and start at

<video src="https://youtube.com/watch?v=zDP9uUMYrvs&t=1523s" title="How We Built Comma" id="youtube-full-with-start-at"/>

## Youtube shortened link

<video src="https://youtu.be/zDP9uUMYrvs" title="How We Built Comma" id="youtube-short"/>

## Youtube shortened link with start at

<video src="https://youtu.be/zDP9uUMYrvs&t=1523s" title="How We Built Comma" id="youtube-short-with-start-at"/>


## Youtube shortened link preview-src

<video src="https://youtu.be/zDP9uUMYrvs" title="How We Built Comma" preview-src="book.jpg" id="youtube-short-with-preview-src"/>

## Youtube shortened link with custom width

<video src="https://youtu.be/zDP9uUMYrvs" title="How We Built Comma" width="300" id="youtube-short-custom-width"/>