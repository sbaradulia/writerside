<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE topic
        SYSTEM "https://helpserver.labs.jb.gg/help/schemas/mvp/html-entities.dtd">
<topic xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:noNamespaceSchemaLocation="https://helpserver.labs.jb.gg/help/schemas/mvp/topic.v2.xsd"
       id="Links-XML"
       title="Links XML">

    <chapter title="Simple anchor" id="chapter-anchor-one">
        <p>
            This is a link to an #anchor element:
            <a id="link-to-anchor" href="Links-summary-XML.topic" anchor="chapter-anchor-first-summary">Simple link</a>.
        </p>
    </chapter>

    <chapter title="Anchors testing summaries" id="chapter-anchors-summary">
        <p>
            This is a link to an #anchor with a summary:
            <a id="link-to-anchor-with-summary" href="Links-summary-XML.topic" anchor="chapter-anchor-first-summary">Link
                with summary</a>.
        </p>

        <p>
            This is a link to an #anchor without summary. Must be displayed default summary:
            <a id="link-to-anchor-without-summary" href="Links-summary-XML.topic"
               anchor="chapter-anchor-without-summary">Link without summary</a>.
        </p>

    </chapter>

    <chapter title="Anchor with chapter summary" id="chapter-anchor-summary">
        <p>
            This is a link to an #anchor with a summary element:
            <a id="link-to-anchor-chapter" href="Links-summary-XML.topic" anchor="chapter-anchor-summary">Link with chapter's summary</a>.
        </p>

    </chapter>

    <chapter title="Link with custom summary" id="chapter-ad-hoc-summary">
        <p>
            This is a link to an the same element as above but with the custom summary:
        </p>
        <a id="link-with-ad-hoc-summary" summary="Ad hoc link summary" href="Links-summary-XML.topic"
           anchor="chapter-anchor-first-summary">Link with ad hoc summary</a>.
    </chapter>

    <chapter title="External link with variable in href" id="chapter-var-in-a-href">
        <var name="jb-ipe" value="https://jb.gg/ipe?extensions="/>
        <p id="p-var-in-href-as-is">
            Here is the link as is: <a id="a-var-in-href-as-is"
                                       href="https://jb.gg/ipe?extensions=com.intellij.annotator"/>
        </p>
        <p id="p-var-in-href-var">
            And <a id="a-var-in-href-var" href="%jb-ipe%com.intellij.annotator">here is the same link using variable in
            href</a>.
        </p>
    </chapter>

    <chapter title="Inline markup in the link text" id="chapter-links-with-inline-markup-in-text">
        <p>
            Strikethrough link for <a href="https://youtrack.jetbrains.com/issue/WRS-97/">WRS-97</a>:
            <a id="link-with-strikethrough" href="https://youtrack.jetbrains.com/issue/WRS-97/">
                <format style="strikethrough">Strikethrough link</format>
            </a>
        </p>
        <p>
            Strikethrough + code link:
            <a id="link-with-strikethrough-code" href="https://google.com">
                <format style="strikethrough"><code>StrikethroughCode link</code></format>
                .
            </a>
        </p>

        <p>
            Emphasised link for <a href="https://youtrack.jetbrains.com/issue/WRS-674">WRS-674</a>:
            <a id="link-emphasised" href="Links-summary-XML.topic">
                <emphasis>Emphasised</emphasis>
                link</a>.
        </p>

        <p>
            Link contains sup:
            <a id="link-sup-markup" href="Links-summary-XML.topic"><sup>Sup</sup> link</a>.
        </p>

        <p>
            Link contains sub:
            <a id="link-sub-markup" href="Links-summary-XML.topic"> <sub>Sub</sub> link</a>.
        </p>

        <p>
            Link contains control:
            <a id="link-control-markup" href="Links-summary-XML.topic">
                <control>Control</control>
                link</a>.
        </p>

        <p>
            Link contains path:
            <a id="link-path-markup" href="Links-summary-XML.topic">
                <path>root/path</path>
                link</a>.
        </p>

        <p>
            Link contains:
            <a id="link-bold-text" href="Links-summary-XML.topic"><b>Bold</b> text</a>.
        </p>

        <p>
            Link contains:
            <a id="link-italic-text" href="Links-summary-XML.topic"><i>Italic</i> text</a>.
        </p>

        <p>
            Link contains:
            <a id="link-underscored-text" href="Links-summary-XML.topic"><u>Underscored</u> text</a>.
        </p>

        <p>
            Link contains:
            <a id="link-strikethrough-text" href="Links-summary-XML.topic"><s>Strikethrough</s> text</a>.
        </p>

        <p>
            Link contains:
            <a id="link-red-text" href="Links-summary-XML.topic">
                <format color="Red">Red</format>
                text</a>.
        </p>
    </chapter>

    <chapter title="Anchor invisible" id="chapter-anchor-invisible">
        <anchor name="my-anchor"/>
        <p>There's an <code>anchor</code> element before this paragraph</p>
    </chapter>

    <chapter title="Chapter with nullable link" id="chapter-nullable-link">
        <p id="excluded-paragraph" instance="!MyProject">This is not available in the current instance</p>
        <a anchor="excluded-paragraph" nullable="true" id="nullable-link">This must appear as plain text</a>
    </chapter>

</topic>