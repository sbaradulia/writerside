# Snippets library MD
{is-library="true"}

<snippet id="runTests">

## Run the Tests

Run the test and make sure it's successful.

</snippet>

<snippet id="note-with-filters">

>  This is filter A.
>
{style="note" id="with-filter-a-note" filter="a"}

>  This is filter B.
>
{style="note" id="with-filter-b-note" filter="b"}

>  This is filter C.
>
{style="note" id="with-filter-c-note" filter="c"}

>  This is filter NOT A.
>
{style="note" id="with-filter-!a-note" filter="!a"}

>  This one is without filters.
>
{style="note" id="with-empty-filters-note"}

</snippet>