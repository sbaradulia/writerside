# Reference-style links MD

Reference-style links use a second set of square brackets,
inside which you place a label of your choosing to identify the link:

This is an example with the [link to Jetbrains.com][id]

You can optionally use a space to separate the sets of brackets:

This is also example of the [JB link] [id] with reference-style.

Then, anywhere in the document, you can define your link label like below.

[id]: https://www.jetbrains.com/  "Optional Title Here"