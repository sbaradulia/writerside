[//]: # (title: Header attributes MD)

## Header level 2

### Header with ID {id="header-2"}

Text just to split the headers in ID testing section

### Header with space in ID {id="header 5"}

Text just to split the headers in ID testing section

### Header with empty string in ID {id=""}

Text just to split the headers in ID testing section

## Header original {title="Header title replaced"}

Text just to split the headers

## Collapsible headers are testing below

### Collapsible = true header {collapsible="true"}

<p id="collapsed-content">You can place details inside the header and collapse it.</p>

### Collapsible = false header {collapsible="false"}

You can place details inside the header but do not collapse it (default state).

### Collapsible header with title replaced via attribute {collapsible="true" title="Original title has been replaced"}

You can place details inside the header, collapse it, and hide original header title.
This is an interesting case.

## Headers with default-state

### Default-state collapsed without collapsible attribute {default-state="collapsed"}

Text just to split the headers in default-state section

### Default-state collapsed with collapsible attribute true
{collapsible="true" default-state="collapsed"}

Text just to split the headers in default-state section

### Default-state expanded with collapsible attribute true {collapsible="true" default-state="expanded"}

Text just to split the headers in default-state section

### Default-state collapsed with collapsible attribute false {collapsible="false" default-state="collapsed"}

Text just to split the headers in default-state section

### Default-state expanded with collapsible attribute false {collapsible="false" default-state="expanded"}

Text just to split the headers in default-state section

## Header capitalization tests

### this is a header or title from topic {caps="title"}

Text just to split the headers in Capitalization section

### header with 2 sentences. new sentence {caps="sentence"}

Text just to split the headers in Capitalization section

### Header in UPPER case
{caps="upper"}

Text just to split the headers in Capitalization section

### HEADER IN lower CASE {caps="lower"}

Text just to split the headers in Capitalization section

### Header as Written {caps="aswritten"}

Text just to split the headers in Capitalization section

### heaDer with NO caps

Text just to split the headers in Capitalization section
