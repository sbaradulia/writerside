[//]: # (title: Red code md)

### Code block of unknown language (WRS-552)

Some text before the code block

```unknown_language
    Some code here
```

## Images with links

Paragraph [![An old rock in the desert](shiprock.jpeg)](https://www.flickr.com/photos/beaurogers/31833779864) paragraph.

## Nested

But visual appearance is not the *only* thing we care about, especially not when claiming something is *accessible*.
In the `following sections` we'll discuss how to take that visual experience to a technical level that we might call
accessible.

<p id="nested-paragraph">

[](Markdown_chapters.md)

</p>

