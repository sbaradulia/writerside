[//]: # (title: Note)

<snippet id="note-before-registration">

>Before registration, read the rules carefully and make sure that your
>goods meet restrictions.
>
{style="note" id="before-registration-note"}

</snippet>

<snippet id="note-with-filters">

>  This is displayed only for the A.
>
{style="note" id="with-filter-a-note" filter="a"}

>  This is displayed only for the B.
>
{style="note" id="with-filter-b-note" filter="b"}

>  This is displayed only for the C.
>
{style="note" id="with-filter-c-note" filter="c"}

>  This is displayed only for not A.
>
{style="note" id="with-filter-!a-note" filter="!a"}

>  This one is without filters.
>
{style="note" id="with-empty-filters-note"}

</snippet>

<snippet id="list">

1. Element one {filter="1"}
2. Element two {filter="2"}
3. Element three {filter="3"}
4. Element four {filter="4"}
5. Element cinco

</snippet>


