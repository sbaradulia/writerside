[//]: # (title: Markdown tabs)

XML tabs may be inserted via live templates.

<tabs>
<tab title="First title">

```
    First snippet
```
</tab>
<tab title="Second title">

```
    Second snippet
```
</tab>
</tabs>
