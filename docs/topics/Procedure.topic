<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE topic SYSTEM "https://helpserver.labs.jb.gg/help/schemas/mvp/html-entities.dtd">

<topic xsi:noNamespaceSchemaLocation="https://helpserver.labs.jb.gg/help/schemas/mvp/topic.v2.xsd"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       title="Procedure"
       id="Procedure">

    <show-structure for="procedure"/>

    <p>Procedures are used to give instructions and explain the steps you need to follow to perform a task.
        Procedures can have an intro wrapped in paragraph and as many steps as you need.</p>

    <p> Procedures are supposed to have a title, but you can also skip one.
        We only recommend doing this if this is a single procedure in a chapter or a topic,
        and the chapter/topic title actually repeats the title you would give to this procedure.
        Note that procedures without titles don’t produce anchors, so you can’t easily link to them,
        and also they cannot be reflected in the in-topic TOC.</p>

    <procedure id="procedure-with-num-steps-without-title" style="steps">
        <step>Step 1.
        </step>
        <step>Step 2.
        </step>
    </procedure>
    <procedure id="procedure-with-choices-without-title" style="choices">
        <step>Option 1.
        </step>
        <step>Option 2.
        </step>
    </procedure>
    <procedure id="procedure-with-title-attr" title="Titled procedure">
        <step>Step 1.
        </step>
        <step>Step 2.
        </step>
    </procedure>
    <procedure id="procedure-with-title-element">
        <title>Titled procedure</title>
        <step>Step 1.
        </step>
        <step>Step 2.
        </step>
    </procedure>

    <procedure id="procedure-collapsible-with-title" title="Titled expanded procedure" style="choices" default-state="expanded" collapsible="true">
        <step>
            Option 1.
        </step>
        <step>
            Option 2.
        </step>
    </procedure>

    <procedure id="procedure-collapsible-with-title-collapsed" title="Title collapsed procedure" style="choices" default-state="collapsed" collapsible="true">
        <step>
            Option 1.
        </step>
        <step>
            Option 2.
        </step>
    </procedure>
    <procedure id="procedure-with-num-steps-with-title-and-titlecase" title="Capitalized titles" style="steps" caps="title">
        <step>Step 1.
        </step>
        <step>Step 2.
        </step>
    </procedure>
    <procedure id="procedure-with-num-steps-with-title-and-lowercase" title="LOWERCASE titles" style="steps" caps="title">
        <step>Step 1.
        </step>
        <step>Step 2.
        </step>
    </procedure>
    <procedure title="Title for section 1" id="switcher-key-titled-procedure-1" style="steps" switcher-key="1">
        <step>Step 1.
        </step>
        <step>Step 2.
        </step>
    </procedure>
    <procedure title="Title for section 2" id="switcher-key-titled-procedure-2" style="steps" switcher-key="2">
        <step>Step 1.
        </step>
        <step>Step 2.
        </step>
    </procedure>
    <procedure title="Title for procedure with switched steps" id="switcher-key-titled-procedure-3" style="steps">
        <step switcher-key="step1-a">Step 1a.
        </step>
        <step switcher-key="step1-b">Step 1b.
        </step>
        <step>Step 2.
        </step>
    </procedure>
    <include from="lib.topic" element-id="procedure-to-include"/>

    <procedure id="procedure-steps-wrs-562" title="Check WRS 562">
        <step>
            <p>Let's check code-block in steps</p>
            <code-block lang="XML">
                <![CDATA[
                <some-tag>
                    <inner-tag/>
                    <p>
                        text text text
                    </p>
                </some-tag>
                ]]>
            </code-block>
        </step>
    </procedure>

</topic>
