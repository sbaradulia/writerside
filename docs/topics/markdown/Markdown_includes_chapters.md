[//]: # (title: Markdown with include of inner chapters)

It is possible to include the whole structure of chapters by a single tag.

<include from="Markdown_snippets_complex.md" element-id="snippet-inner-chapter"/>

Some text for an epilogue.  


