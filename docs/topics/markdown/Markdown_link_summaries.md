[//]: # (title: Markdown link summaries)

<link-summary>This is summary element for a whole Markdown topic</link-summary>

The "pure" Markdown does not have link summaries, but we can use SXML blocks for that.

## Inner Chapter 1 {id="inner-chapter-1"}

<link-summary>Summary element for the Markdown chapter</link-summary>

Text for chapter 1

## Inner chapter 2 {id="inner-chapter-2"}

Note how the `link-summary` element MUST be the first element in the chapter

<link-summary>Thus, this link summary does not work</link-summary>

Some more text for chapter 2.

