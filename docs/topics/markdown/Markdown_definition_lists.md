[//]: # (title: Markdown definition lists)

Below is the MD definition list.

Definition Term  
: Definition for Definition Term

Second Term, Definition Has Formatting
: Definition with **bold** and *italic*

Now ***Both_*** Term and Definition are with `format()`
: Definition with ~~strikethrough~~ text


Another Term, Definition Has Attributes
: Definition with Attributes {id="definition-attributes-1" xxx="yyy"}

Term With Many Definitions
: Definition of above term
: Another definition of above term

Term With ID {id="id-for-term"}
: Definition for "Term with ID"

Multi-line term. Here is the first line.
Here is the second line of the same term.
: Definition of the multi-line term.

It may look like a multi-line term, but it is not.

Here is just a normal one-line term
: Definition of the one-line term

Below is the XML definition list.

<deflist style="wide" id="sorted" sorted="desc">
            <def title="Alice">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
                    et dolore magna aliqua.
                </p>
            </def>
            <def title="Bob">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
                    et dolore magna aliqua.
                </p>
            </def>
        </deflist>