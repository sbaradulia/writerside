---
link-summary: This is a summary for the link.
card-summary: This is a summary for the card.
web-summary: This is a summary for web crawlers.
---
[//]: # (title: Frontmatter for summaries, standard markup below for title)

In markdown-based SSGs ofter used YAML-styled Front Matter as a way to add structured data to a page. 
We can do the same for summary elements.