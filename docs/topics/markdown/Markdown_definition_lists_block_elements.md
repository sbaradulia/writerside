[//]: # (title: Markdown definition lists)

The block elements inside definitions were problematic for us at some point 

## List inside the definition.

Java EE plugins split - list in the definition
: Plugin `com.intellij.javaee` _Java EE: EJB, JPA, Servlets_ has been split to:
- piece 1
- piece 2
- `some code` _formatted piece 3_
- _more pieces_

## Code block inside the definition.

Example term
: 
```
org.jetbrains.intellij.buildFeature.buildSearchableOptions=false
```
