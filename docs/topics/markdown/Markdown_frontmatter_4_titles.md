---
name: This is also a topic title.
explanation: in WRS, the `name` property inside frontmatter is considered the same as a `title` property for legacy reasons
---

# Frontmatter for generic title, first-level Markdown heading with filtering attrbutes for instance-specific title
{ instance="n" }

# Frontmatter for generic title, many first-level Markdown headings for instance-specific titles. This is for md
{ instance="md" }

If an instance-specific title is given, it should override the generic ones.
