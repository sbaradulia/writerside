# Light-dark image test

The picture must change when user switches the theme.  
The light theme shows the light is on, the dark theme shows the light is off.

![](../../imageStock/Boy.jpeg){id="image"}
