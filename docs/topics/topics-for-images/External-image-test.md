# External image test

External image under JB gateway **will not be displayed** in Preview and in published docs:

![](https://images.writerside.pages.jetbrains.team/images/U2.jpeg)

Just the same link to image

[](https://images.writerside.pages.jetbrains.team/images/U2.jpeg)
